const express = require('express')
const app = express()
const bodyParser = require("body-parser")
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true})

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error'))

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const DeploymentSchema = new mongoose.Schema({
	url: String, 
	templateName: String, 
	version: String, 
	deployedAt: String
})

const Deployment = mongoose.model('Deployment', DeploymentSchema)

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Methods", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  res.header('Content-Type', 'application/json')
  next();
});

app.get('/api/deployments', function(req, res) {
	Deployment.find(function (err, depls) {
	  if (err) {
		  res.status(400)
		  res.send(JSON.stringify({result: 'error', message: err}))
	  }
	  res.end(JSON.stringify(depls))
	})
})

app.post('/api/deployment', function(req, res) {
	const depl = new Deployment({
		url: req.body.url, 
		templateName: req.body.templateName, 
		version: req.body.version, 
		deployedAt: new Date().toLocaleString()
	})
	depl.save(function (err) {
		if (err) {
			res.status(400)
			res.send(JSON.stringify({result: 'error', message: err}))
		}
	})
	res.send(JSON.stringify({result: 'ok', data: depl}))
})

app.delete('/api/deployment/:id', function (req, res) {
	Deployment.deleteOne({ _id: req.params.id }, function (err) {
		if (err) {
			res.status(400)
			res.send(JSON.stringify({result: 'error', message: err}))	
		}
		res.send(JSON.stringify({result: 'ok'}))
	})
})

app.listen(3001)